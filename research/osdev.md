# Research

## OS

- https://wiki.osdev.org/Bare_bones

## Debug
- https://wiki.osdev.org/Kernel_Debugging

# Registers
- https://wiki.osdev.org/CPU_Registers_x86-64

## ELF
- https://wiki.osdev.org/ELF

## VGA
- https://wiki.osdev.org/Printing_to_Screen
- https://wiki.osdev.org/Text_UI
- https://wiki.osdev.org/VGA_Hardware

## Global Constructors
- https://wiki.osdev.org/Calling_Global_Constructors