# SOFTWARE
TARGET := i686-elf-
GCC := $(TARGET)gcc
LD := $(TARGET)ld
AS := $(TARGET)as
QEMU := qemu-system-i386
READELF := $(TARGET)readelf
OBJCOPY := $(TARGET)objcopy
OBJDUMP := $(TARGET)objdump

# Compiled objects
BUILD_DIR = build
# Source code
SRC_DIR = src
# Debug
DEBUG_DIR = debug
# Include
INCLUDE_DIR = $(BUILD_DIR)/include
# Output os file (final product)
OS=my_os
OS_BIN=$(OS).bin

# COLORS
BLACK		:= $(shell tput -Txterm setaf 0)
RED			:= $(shell tput -Txterm setaf 1)
GREEN		:= $(shell tput -Txterm setaf 2)
YELLOW		:= $(shell tput -Txterm setaf 3)
LIGHTPURPLE	:= $(shell tput -Txterm setaf 4)
PURPLE		:= $(shell tput -Txterm setaf 5)
BLUE		:= $(shell tput -Txterm setaf 6)
WHITE		:= $(shell tput -Txterm setaf 7)
RESET		:= $(shell tput -Txterm sgr0)
# END COLORS

# FLAGS
#	-g
#		This will add all the debugging symbols
#	-std=gnu99
#		GNU dialect of ISO C99
#	-O2
#		Optimize even more
#	-Wextra
#		Print extra warning messages
#	-Wall
#		Show all warnings.
#	-ffreestanding
#		A freestanding environment is an environment in which the standard library may not exist,
#		and program startup may not necessarily be at main.
#		The option -ffreestanding directs the compiler to not assume that standard functions have
#		their usual definition.
#	-Iinclude
#		Search for header files in the include folder.
GCC_FLAGS := -std=gnu99 -ffreestanding -O2 -Wall -Wextra -g -I$(INCLUDE_DIR)
AS_FLAGS := -g
# Flags for linking
#	-nostdlib
#		No startup files and only the libraries you specify are passed to the linker.
#	-nostartfiles
#		Do not use the standard system startup files when linking.
LD_FLAGS := -ffreestanding -O2 -nostdlib -Xlinker -Map=$(DEBUG_DIR)/output.map

# Array of objects from C
C_FILES = $(shell find $(SRC_DIR) -name '*.c')
OBJ_FILES = $(C_FILES:$(SRC_DIR)/%.c=$(BUILD_DIR)/%_c.o)

# Array of objects from assembly
AS_FILES = $(shell find $(SRC_DIR) -name '*.s')
OBJ_FILES += $(AS_FILES:$(SRC_DIR)/%.s=$(BUILD_DIR)/%_s.o)

# Array of header files
H_FILES = $(shell find $(SRC_DIR) -name '*.h')
H_INCLUDE = $(H_FILES:$(SRC_DIR)/%.h=$(INCLUDE_DIR)/%.h)

# Calling Global Constructors
CRTI_OBJ = $(BUILD_DIR)/kernel/boot/crti_s.o
CRTBEGIN_OBJ := $(shell $(GCC) $(GCC_FLAGS) -print-file-name=crtbegin.o)
CRTEND_OBJ := $(shell $(GCC) $(GCC_FLAGS) -print-file-name=crtend.o)
CRTN_OBJ = $(BUILD_DIR)/kernel/boot/crtn_s.o

OBJ_FILES := $(filter-out $(CRTI_OBJ) $(CRTN_OBJ), $(OBJ_FILES))
OBJ_FILES := $(CRTI_OBJ) $(CRTBEGIN_OBJ) $(OBJ_FILES) $(CRTEND_OBJ) $(CRTN_OBJ)

all: folders $(OS_BIN) debug_files

folders:
# Create debug folder
	@mkdir -p $(DEBUG_DIR)

# BUILD BEGIN
# Compile C files
$(BUILD_DIR)/%_c.o: $(SRC_DIR)/%.c
	@mkdir -p $(@D)
	$(info $(RED)[gcc]$(RESET) $< => $@)
	@$(GCC) $(GCC_FLAGS) -c $< -o $@

# Compile asembler files
$(BUILD_DIR)/%_s.o: $(SRC_DIR)/%.s
	@mkdir -p $(@D)

	@$(info $(RED)[as]$(RESET) $< => $@)
	@$(AS) $(AS_FLAGS) -c $< -o $@

# Copy header files to include folder
$(INCLUDE_DIR)/%.h: $(SRC_DIR)/%.h
	@mkdir -p $(@D)
	$(info $(RED)[cp]$(RESET) $< => $@)
	@cp $< $@

$(OS_BIN): $(H_INCLUDE) $(SRC_DIR)/linker.ld $(OBJ_FILES)
	$(info $(RED)[ld]$(RESET) $(BUILD_DIR)/*.o => $@)
	@$(GCC) $(LD_FLAGS) -T $(SRC_DIR)/linker.ld -o $@ $(OBJ_FILES) -lgcc
# BUILD END

# DEBUG BEGIN
debug_files: $(DEBUG_DIR)/$(OS).sym $(DEBUG_DIR)/$(OS).dump debug_sections

$(DEBUG_DIR)/$(OS).sym:
# Put all of the debugging information in $(OS).sym
	$(info $(RED)[objcopy]$(RESET) $(OS_BIN) => $@)
	@$(OBJCOPY) --only-keep-debug $(OS_BIN) $@

# No longer need debug information in the file
#	$(info [objcopy] --strip-debug)
#	@$(OBJCOPY) --strip-debug $(OS_BIN)

$(DEBUG_DIR)/$(OS).dump: $(OS_BIN)
	$(info $(RED)[objdump]$(RESET) $< => $@)
	@$(OBJDUMP) -D $< > $@


debug_sections: $(DEBUG_DIR)/$(OS).sections \
$(DEBUG_DIR)/$(OS).txt \
$(DEBUG_DIR)/$(OS)_data.txt \
$(DEBUG_DIR)/$(OS)_rodata.txt
# Sections
# Headers
$(DEBUG_DIR)/$(OS).sections: $(OS_BIN)
	$(info $(RED)[readelf]$(RESET) $< => $@)
	@$(READELF) -S $< > $@

$(DEBUG_DIR)/$(OS).txt: $(OS_BIN)
	$(info $(RED)[objdump]$(RESET) $< => $@)
	@$(OBJDUMP) -drS $< > $@

$(DEBUG_DIR)/$(OS)_data.txt: $(OS_BIN)
	$(info $(RED)[objdump]$(RESET) $< => $@)
	@$(OBJDUMP) -s -j .data $< > $@

$(DEBUG_DIR)/$(OS)_rodata.txt: $(OS_BIN)
	$(info $(RED)[objdump]$(RESET) $< => $@)
	@-$(OBJDUMP) -s -j .rodata $< > $@ 2>/dev/null; true
# DEBUG END

run:
	@$(QEMU) \
	-kernel $(OS_BIN)

debug:
	$(QEMU) \
	-kernel $(OS_BIN) \
	-s -S &
	gdb $(DEBUG_DIR)/$(OS).sym -x .gdbinit
clean :
	rm -rf $(BUILD_DIR)
	rm -rf $(DEBUG_DIR)
	rm -rf *.bin

.PHONY: debug