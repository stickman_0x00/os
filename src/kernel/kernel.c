#include "console/console.h"
#include <stddef.h>

/* Check if the compiler thinks you are targeting the wrong operating system. */
#if defined(__linux__)
	#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif

/* This tutorial will only work for the 32-bit ix86 targets. */
#if !defined(__i386__)
	#error "This tutorial needs to be compiled with a ix86-elf compiler"
#endif

__attribute__((constructor))
void init(void)
{
	console_init();
	console_print("Kernel constructor\n");
}

void kernel_init(void)
{
	console_print("Kernel init\n");
}

void kernel_main(void)
{
	kernel_init();
	console_print("d[0.0]b\n");

	console_print("Kernel hang...\n");
	while (1);
}