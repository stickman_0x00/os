#include "console.h"
#include "kernel/devices/vga/vga.h"
#include "kernel/utils.h"
#include <stddef.h>
#include <stdint.h>

// Console buffer width size.
#define WIDTH VGA_WIDTH
// Console buffer heigth size.
#define HEIGHT VGA_HEIGHT

// Console buffer size.
#define BUFFER_SIZE (WIDTH * HEIGHT)

// Cursor position.
static size_t column; // x
static size_t row; // y

// Clean present row.
static inline void clean_line(void)
{
	// Loop every column and print ' ' character.
	for (size_t x = 0; x < WIDTH; x++)
		vga_print(' ', x, row);
}

// Move all rows up one row and discard the upper most, and leave a
// blank row at the bottom ready to be filled up with characters.
static void scroll_down(void)
{
	// Loop every position.
	for (size_t y = 1; y < HEIGHT; y++)
		for (size_t x = 0; x < WIDTH; x++)
		{
			const unsigned char c = vga_get_char(x,y);
			vga_print(c, x, y-1);
		}

	clean_line();
}

// Calculate next row.
static void next_row(void)
{
	if (++row < HEIGHT)
		return;

	row--;

	scroll_down();
}

// Set next position.
static void next_column(void)
{
	if (++column < WIDTH)
		return;

	// Reach the limit of columns, reset columns.
	column = 0;

	next_row();
}

static void print_char(const char c)
{
	switch (c)
	{
		case '\n': // Paragraph
			column = 0;

			next_row();
		return;
		case '\t': // Tab
			for (size_t count = 0; count < TAB_SIZE; count++)
				next_column();
		return;
	}

	vga_print(c, column, row);
	next_column();
}

// Print string.
void console_print(const char *str)
{
	while(*str)
	{
		print_char(*str);
		str++;
	}
}

// Clean screen and set position to (0,0).
void console_clean(void)
{
	// Set initial position;
	row = 0;
	column = 0;

	// Clean entire buffer.
	for (size_t i = 0; i < BUFFER_SIZE; i++)
			print_char(' ');

	row = 0;
	column = 0;
}

void console_init(void)
{
	vga_init();

	console_clean();
}