#ifndef _CONSOLE_H
#define _CONSOLE_H

void console_clean(void);
void console_print(const char *string);
void console_init(void);

#endif