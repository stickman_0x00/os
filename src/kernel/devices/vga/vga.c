#include "vga.h"
#include <stddef.h>
#include <stdint.h>

// VGA mode 3, the linear text buffer is located in physical at 0xB8000.
#define VGA_BASE 0xb8000

#define BUFFER ((uint16_t *) (VGA_BASE))

static size_t color;

/*
	8 bit
	+-----+----------+----------+
	|  7  |  6 5 4   |  3 2 1 0 |
	|Blink|Background|Foreground|
	+-----+----------+----------+
*/
// Set foreground and background color.
void vga_set_color(const enum vga_color fg, enum vga_color bg)
{
	color = fg | bg << 4;
}

/*
	16 bit
	+-------+------------+------------+-----------------+
	|  15   |  14 13 12  |  11 10 9 8 | 7 6 5 4 3 2 1 0 |
	+-------+------------+------------+-----------------+
	| Blink | Background | Foreground |    Character    |
	+-------+------------+------------+-----------------+
	|   Attribute byte   |       ASCII code byte        |
	+-------+------------+------------+-----------------+
*/
// Transform character and return 2 bytes "character" with color.
static inline uint16_t vga_entry(unsigned char uc)
{
	return (uint16_t) uc | (uint16_t) color << 8;
}

unsigned char vga_get_char(size_t x, size_t y)
{
	return BUFFER[y * VGA_WIDTH + x];
}

enum vga_color vga_get_foreground(size_t x, size_t y)
{
	return (BUFFER[y * VGA_WIDTH + x] >> 8) & 0b1111;
}

enum vga_color vga_get_background(size_t x, size_t y)
{
	return (BUFFER[y * VGA_WIDTH + x] >> 12) & 0b1111;
}

void vga_print(unsigned char c, size_t x, size_t y)
{
	const size_t index = y * VGA_WIDTH + x;
	BUFFER[index] = vga_entry(c);
}

void vga_init(void)
{
	// Set default colors FONT: LIGH_GREY, BACKGROUND: BLACK.
	vga_set_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
}

